setwd("C:/Users/neriya/Desktop/task3")
house.row <- read.csv('house_data.csv')
install.packages('ggplot2')
library(ggplot2)
install.packages('caTools') 
library(caTools) 

summary(house.row)
house.prepared <- house.row
house.prepared$date <- as.character(house.prepared$date)

str(house.prepared)

Sys.setlocale("LC_TIME","English")
day.in.month <- substr(house.prepared$date,7,8)
year <- substr(house.prepared$date,1,4)
month <- substr(house.prepared$date,5,6)
house.prepared$dayInMonth <- as.factor(day.in.month)
house.prepared$month <- as.factor(month)
house.prepared$year <- as.factor(year)
house.prepared$bedrooms<-as.factor(house.prepared$bedrooms)
house.prepared$bathrooms<-as.factor(house.prepared$bathrooms)
house.prepared$floors<-as.factor(house.prepared$floors)
house.prepared$waterfront<-as.factor(house.prepared$waterfront)
house.prepared$view <-as.factor(house.prepared$view )
house.prepared$condition <-as.factor(house.prepared$condition )
house.prepared$grade<-as.factor(house.prepared$ grade)

house.prepared$id<-NULL
house.prepared$long<-NULL
house.prepared$lat<-NULL
house.prepared$zipcode<-NULL
house.prepared$date<-NULL

str(house.prepared)

ggplot(house.prepared,aes(bedrooms ,price)) + geom_boxplot()#����� ���
ggplot(house.prepared,aes(bathrooms,price)) + geom_boxplot()#����-����� �� ��� �����
ggplot(house.prepared,aes(waterfront,price)) + geom_boxplot()#�����
ggplot(house.prepared,aes(condition ,price)) + geom_boxplot()#�����
ggplot(house.prepared,aes(grade,price)) + geom_boxplot()#����� ����
ggplot(house.prepared,aes(dayInMonth,price)) + geom_boxplot()#�� �����
ggplot(house.prepared,aes(year,price)) + geom_boxplot()#�� �����
ggplot(house.prepared,aes(month,price)) + geom_boxplot()#�� �����
ggplot(house.prepared,aes(floors,price)) + geom_boxplot()#�� �����
ggplot(house.prepared,aes(view,price)) + geom_boxplot() #�����

house.prepared$bathrooms<-as.integer(house.prepared$bathrooms)

ggplot(house.prepared,aes(bathrooms ,price)) + geom_point() + stat_smooth(method = lm)#����� ���
ggplot(house.prepared,aes(sqft_living ,price)) + geom_point() + stat_smooth(method = lm)#�����
ggplot(house.prepared,aes(sqft_lot  ,price)) + geom_point() + stat_smooth(method = lm)#�����
ggplot(house.prepared,aes(sqft_above ,price)) + geom_point() + stat_smooth(method = lm)#�����
ggplot(house.prepared,aes(sqft_basement ,price)) + geom_point() + stat_smooth(method = lm)#�����
ggplot(house.prepared,aes(yr_built ,price)) + geom_point() + stat_smooth(method = lm)#�� �����
ggplot(house.prepared,aes(yr_renovated ,price)) + geom_point() + stat_smooth(method = lm)#�� �����
ggplot(house.prepared,aes(sqft_living15 ,price)) + geom_point() + stat_smooth(method = lm)#�����
ggplot(house.prepared,aes(sqft_lot15  ,price)) + geom_point() + stat_smooth(method = lm)#����� ���


house.prepared$dayInMonth<-NULL
house.prepared$year<-NULL
house.prepared$month<-NULL
house.prepared$floors<-NULL
house.prepared$yr_built<-NULL
house.prepared$yr_renovated<-NULL

str(house.prepared)

filter <- sample.split(house.prepared$price, SplitRatio = 0.7)
house.train <- subset(house.prepared, filter == TRUE)
house.test <- subset(house.prepared, filter == FALSE)

model <-lm(price~. , house.train)
summary(model)

predicted.train <- predict(model,house.train)
predicted.test <- predict(model,house.test)

MSE.train <- mean((house.train$price - predicted.train)**2)
MSE.test <- mean((house.test$price - predicted.test)**2)
error.train<-(MSE.train)**0.5
error.test<-(MSE.test)**0.5

error.train
error.test
